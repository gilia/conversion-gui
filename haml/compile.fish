#! /usr/bin/fish

set -l prevdir "$PWD"
if test -d haml
    cd haml
end

function compile_haml --description="Compile the haml file"
    set -l basename (basename "$argv[1]" .haml)
    echo $basename
    haml -f xhtml "$argv[1]" > "../$basename.html"
end

if test -z "$argv[1]"
    for file in *.haml
        compile_haml $file
    end
else
    if test -f $argv[1]
        compile_haml $argv[1]
    else
        echo "File $argv[1] does not exists"
    end
end

cd "$prevdir"
