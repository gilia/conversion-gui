# conversor.coffee --
# Copyright (C) 2019 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import {load_example} from './example.js'

reasoning_URL = "../reasoning/"

# Remove html tags characters in order to display the xml code as HTML string.
#
# @param text {string} The XML as string.
# @return {string} The HTML string.
sanitize_xml = (text) ->
    ltc = /</gi
    gtc = />/gi
    nl = /\n/gi
    tab = /([\t    ])/gi
    text = text.replace ltc, "&lt;"
    text = text.replace gtc, "&gt;"
    text = text.replace nl, '<br/>'
    text = text.replace tab,  '&nbsp;'

    text

unsanitize_xml = (text) ->
    text = text.replace '&lt;', '<'
    text = text.replace '&gt;', '>'
    text = text.replace '<br/>', "\n"

    text

# Update the output textarea with the received data.
# Do syntax highlight too.
#
# @param text {string} The result text.
# @param lang {string} The language of the text. Ex.: ~html~, ~xml~, ~json~,
#   etc.
update_results = (text, lang) ->
    text = sanitize_xml text unless lang == 'html'
    tag = $("#output")
    tag.attr('class', lang)
    tag.text ''
    tag.append text
    hljs.highlightBlock tag[0]

update_validation_results = (json) ->
    tag = $("#output")
    tag.attr 'class', 'json'
    tag.append json
    hljs.highlightBlock tag[0]

request_json = (type, options={}) ->
    if type == "v1tov2"
        url = reasoning_URL + "json2json/?from=v1&to=v2"
    else
        url = reasoning_URL + "json2json/?from=v2&to=v1"

    if options.prefix_iri?
        if options.prefix_iri
            url += "&prefix_iri=true"
        else
            url += "&prefix_iri=false"

    $.post url, {
        json: $("#input").val()
        },
        (result, status, jqxhr) ->
            update_results result, 'json'

request_html = (options={}) ->
    url = reasoning_URL + 'translate/berardi.php?format=html'

    if options.prefix_iri?
        if options.prefix_iri
            url += "&prefix_iri=true"
        else
            url += "&prefix_iri=false"

    $.post url, {
        json: $("#input").val()
        },
        (result, status, jqxhr) ->
            update_results result, 'html'


request_owllink = (options={}) ->
    url = reasoning_URL + 'translate/berardi.php?'

    if options.prefix_iri?
        if options.prefix_iri
            url += "prefix_iri=true"
        else
            url += "prefix_iri=false"

    $.post url, { json: $("#input").val() },
        (result, status, jqxhr) ->
            update_results result, 'xml'
        , 'text'

request_owlxml = (options={}) ->
    url = reasoning_URL + 'translate/owl2.php?strategy=berardi'

    if options.prefix_iri?
        if options.prefix_iri
            url += "&prefix_iri=true"
        else
            url += "&prefix_iri=false"

    $.post url, { json: $("#input").val() },
        (result, status, jqxhr) ->
            update_results result, 'xml'
        , 'text'

# Request a JSON validation from the server
#
# @param version {string} "v1", "v2" or other version recognized
#   by the server.
request_validation = (version) ->
    url = reasoning_URL + "json_validators/index.php?version=" + version

    $.post url, {
        json: $("#input").val()
        },
        (result, status, jqxhr) ->
            update_validation_results result

on_convert_click = () ->
    $("#output").html ''

    convertion = $("#slt-to").val()
    options =
        prefix_iri: $("#prefix_iri")[0].checked

    if convertion == "v1tov2" or convertion == "v2tov1"
        request_json convertion, options
    else if convertion == "html"
        request_html options
    else if convertion == "owllink"
        request_owllink options
    else if convertion == "owlxml"
        request_owlxml options

on_validate_click = () ->
    $("#output").html ''
    convertion = $("#slt-to").val()
    if convertion in ["v1tov2", "html", "owllink"]
        convertion = "v1"
    else
        convertion = "v2"
    
    request_validation convertion

update_example = (example_str) ->
    $("textarea#input").val example_str

on_load_example_click = () ->
    example_name = $("#slt-examples").val()
    console.log "load requested:" + example_name
    load_example example_name, update_example

on_beautify_click = () ->
    text = $("textarea#input").val()
    obj = JSON.parse text
    text = JSON.stringify obj, null, 2
    $("textarea#input").val text 

on_xml_beautify_click = () ->
    if $("#output").attr('class') == 'xml'
        text = $("code#output").text()
        text = unsanitize_xml text
        format = require 'xml-formatter'
        text = format text
        console.log text
        update_results text, 'xml'

on_copy_output_click = () ->
    text = $("<textarea>").val $("code#output").text()
    text.show()
    $(".server-output").append text
    text.select()
    document.execCommand "copy"
    console.log "copied"
    text.remove()

# @namespace convert
assign_events = () ->
    $("#btn-convert").on 'click', () ->
        on_convert_click()
    $("#btn-validate").on 'click', () ->
        on_validate_click()
    $("#btn-beautify").on 'click', () ->
        on_beautify_click()
    $("#btn-beautify-output").on 'click', () ->
        on_xml_beautify_click()
    $("#btn-copy-output").on 'click', () ->
        on_copy_output_click()
    $("#btn-example").on 'click', () ->
        on_load_example_click()

$.when($.ready).then () ->
    assign_events()
    console.log "index initialized"

