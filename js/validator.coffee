# validator.coffee --
# Copyright (C) 2020 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import {load_example} from './example.js'
import {request_validation} from './validator_server.js'

clear_error_list = () ->
    tbody = $("#details").find "tbody"
    tbody.html ""

add_error = (err_str) ->
    tbody = $("#details").find "tbody"
    tr = $("<tr>")
    th = $("<td>").html err_str
    tr.append th
    tbody.append tr

update_valid = (json_result) ->
    if json_result.valid
        $("#valid").html "The input is valid"
        $("#valid").attr 'class', 'alert alert-success'
    else
        $("#valid").html "The input is invalid"
        $("#valid").attr 'class', 'alert alert-danger'

update_output = (result) ->
    $("#output").html result
    json = JSON.parse result

    update_valid json

    clear_error_list()
    add_error err for err in json.errors

update_example = (example_str) ->
    $("textarea#input").text example_str

on_load_example_click = () ->
    example_name = $("#slt-examples").val()
    console.log "load requested:" + example_name
    load_example example_name, update_example

on_beautify_click = () ->
    text = $("textarea#input").text()
    obj = JSON.parse text
    text = JSON.stringify obj, null, 2
    $("textarea#input").text text 

on_validate_click = () ->
    version = $("#slt-schema").val()
    show_loading()
    request_validation version, (result) =>
        hide_loading()
        update_output result

show_loading = () ->
    $(".validator-output").hide()
    $("#btn-validate .spinner-grow").show()
    $("#loading").show()

hide_loading = (with_answer = true) ->
    if with_answer
        $(".validator-output").show()
    else
        $(".validator-output").hide()
    $("#btn-validate .spinner-grow").hide()
    $("#loading").hide()

on_copy_output_click = () ->
    text = $("<textarea>").text $("code#output").text()
    text.show()
    $(".validator-output").append text
    text.select()
    document.execCommand "copy"
    console.log "copied"
    text.remove()

assign_events = () ->
    $("#btn-example").on 'click', () ->
        on_load_example_click()
    $("#btn-beautify").on 'click', () ->
        on_beautify_click()
    $("#btn-copy-output").on 'click', () ->
        on_copy_output_click()
    $("#btn-validate").on 'click', () ->
        on_validate_click()

$.when($.ready).then () ->
    assign_events()
    hide_loading false
    console.log "validator initialized"
