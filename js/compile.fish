#! /usr/bin/fish

set -l prevdir "$PWD"

if test -d js
    cd js
end

coffee -c *.coffee

cd "$prevdir"
