# server.coffee --
# Copyright (C) 2019 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Server
#
# Represents the server API for consistency checks.
#
# @namespace semcheck
export class Server

    constructor: (url) ->
        @url = url
        @translate_suffix =
            berardi: '/reasoning/translate/berardi.php'
            crowd: '/reasoning/translate/crowd.php'
        @check_suffix = '/reasoning/querying/satisfiable.php'
        @answer_event = (answer) ->
            console.log answer
        @internal_answer_event = (answer) =>
            @output = answer
            @answer_event answer
        @reasoner = 'Konclude'
        @encoding = 'berardi'
        @jsonver = 2
        @input = null
        @output = null

    generate_url: () ->
        @url + @check_suffix + '?' + 'json_version=' + @jsonver

    generate_post_data: () ->
        json: @input
        reasoner: @reasoner
        encoding: @encoding
        
    # Send a diagram representation to the server.
    # 
    # @return [string] A JSON response from the server.
    do_request: () ->
        $.post @generate_url(),
            @generate_post_data(),
            (result) =>
                @internal_answer_event result

    # Request the translation of the diagram into OWL 2.
    # 
    # @param conversion [string] A type of conversion to use
    # @return [string] The OWL 2 (response from the server).
    request_translation: (diagram, conversion="berardi") ->
        $.post @url + @translate_suffix[conversion] + '?json_version=1',
            json: JSON.stringify(diagram)


prefix = 'http://crowd.fi.uncoma.edu.ar/kb1#'

iris2names = (iris) ->
    for curl in iris
        do (curl) ->
            curl.replace semcheck.prefix, ''    

# Process the server answer
process_answer = (result) ->
    json = JSON.parse result
    console.log json
    answer = json.answer

    satisf = semcheck.iris2names answer.satisfiable.classes
    insatisf = semcheck.iris2names answer.unsatisfiable.classes
    modelcheck.mark_all satisf, insatisf

# An instance of semcheck.Server
# 
# @namespace semcheck
export iserver = new Server('../')

export request_service = (data, callback) ->
    iserver.input = data.input
    iserver.jsonver = data.json_version
    iserver.owllink = data.owllink
    iserver.reasoner = data.reasoner

    iserver.answer_event = callback
    iserver.do_request()

