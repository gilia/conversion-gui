# reasoning.coffee --
# Copyright (C) 2019 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import {load_example} from './example.js'
import {sanitize_xml} from './utils.js'
import {request_service} from './server.js'

show_output = (answer) ->
    divsatisf = $(".satisfiable")
    codesatisf = divsatisf.find '#satisfiable'
    divrinput = $(".reasoner-input")
    coderinput = divrinput.find '#reasoner-input'
    divroutput = $(".reasoner-output")
    coderoutput = divroutput.find '#reasoner-output'
    divoutput = $("#output")
    $('.errors').hide()
    
    try
        jsonanswer = JSON.parse answer
        console.log jsonanswer

        codesatisf.attr 'class', 'json'
        codesatisf.html JSON.stringify jsonanswer.answer.satisfiable
        hljs.highlightBlock codesatisf[0]

        coderinput.attr 'class', 'xml'
        coderinput.html sanitize_xml jsonanswer.answer.reasoner.input
        hljs.highlightBlock coderinput[0]

        coderoutput.attr 'class', 'xml'
        coderoutput.html sanitize_xml jsonanswer.answer.reasoner.output
        hljs.highlightBlock coderoutput[0]

    catch error
        divsatisf.hide()
        divrinput.hide()
        divroutput.hide()
        console.log error
        $('.errors').show()
        $('#errors').html error
    
    divoutput.attr 'class', 'json'
    divoutput.html sanitize_xml answer
    hljs.highlightBlock divoutput[0]    

# What to do
export request_reasoning_clicked = () ->
    data =
        input: $('#input').val()
        json_version: $('#json-version').val()
        owllink: $('#owllink').val()
        checktype: $('#check-type').val()
        reasoner: $('#reasoner').val()

    console.log data
    show_loading()

    request_service data, (answer) ->
            console.log 'recieved'
            show_output answer
            hide_loading true

show_loading = () ->
    $(".reasoner-answer").hide()
    $("#btn-request .spinner-grow").show()
    $("#loading").show()

hide_loading = (with_answer=true) ->
    if with_answer
        $(".reasoner-answer").show()
    else
        $(".reasoner-answer").hide()
    $("#btn-request .spinner-grow").hide()
    $("#loading").hide()

update_example = (example_str) ->
    $("textarea#input").text example_str

on_load_example_click = () ->
    example_name = $("#slt-examples").val()
    console.log "load requested:" + example_name
    load_example example_name, update_example

on_beautify_click = () ->
    text = $("textarea#input").text()
    obj = JSON.parse text
    text = JSON.stringify obj, null, 2
    $("textarea#input").text text 

on_copy_output_click = () ->
    text = $("<textarea>").text $("code#output").text()
    text.show()
    $(".server-output").append text
    text.select()
    document.execCommand "copy"
    console.log "copied"
    text.remove()

on_snippets_click = (obj_clicked) ->
    if obj_clicked.classList.contains "dropdown-toggle"
        return
    snippet_name = obj_clicked.textContent.trim()
    script = $("script#" + snippet_name)
    textarea = $("textarea#owllink")
    textarea.val textarea.val() + "\n" +  script.html().trim()
    

assign_events = () ->
    $("#owllink-snippets").on 'click', (evt) ->
        on_snippets_click evt.target
    $("#btn-request").on 'click', () ->
        request_reasoning_clicked()
    $("#btn-beautify").on 'click', () ->
        on_beautify_click()
    $("#btn-example").on 'click', () ->
        on_load_example_click()
    $("#btn-copy-output").on 'click', () ->
        on_copy_output_click()
    console.log "events assigned"

$.when($.ready).then () ->
    assign_events()
    hide_loading false
    console.log 'reasoning initializer'
