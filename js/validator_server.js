// Generated by CoffeeScript 2.4.1
// validator_server.coffee --
// Copyright (C) 2020 GILIA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
var validator_URL;

validator_URL = "../reasoning/json_validators/index.php";

// Request a JSON validation from the server

// @param version {string} "v1", "v2" or other version recognized
//   by the server.
// @param callback {function} A callback that is called when the validation
//   has an answer. It receives the JSON result as a parameter.
export var request_validation = function(version, callback) {
  var url;
  url = validator_URL + "?version=" + version;
  return $.post(url, {
    json: $("#input").val()
  }, (result, status, jqxhr) => {
    return callback(result);
  });
};
